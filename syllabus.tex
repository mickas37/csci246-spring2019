\documentclass[english]{article}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{xcolor}
\newcommand{\todo}[1]{{\color{red}TODO(XXX: #1)}}
%------------------------------ Text -------------------------------------
\author{Instructor: Samuel A. Micka \\ TA: Brad McCoy}
\title{Syllabus: CSCI 246 Spring 2019}
\begin{document}
\thispagestyle{empty}
\maketitle

%------------------------------ Section -------------------------------------
\section*{Time and Meeting Place}
We will meet in REID $402$ from $8$:$00$-$8$:$50$ on Monday,
Wednesday, and Friday.

%------------------------------ Section -------------------------------------
\section*{Contacting the Instructor and TA}
You can talk to Sam in person during his office hours in Barnard 350:
\begin{itemize}
	\item T, TH $16$:$00$-$17$:$00$
	\item W $9$:$00$-$10$:$00$
\end{itemize}
Additionally, you can reach Sam on D2L or via. email:
\href{mailto:samuelmicka@montana.edu}{samuelmicka@montana.edu} or just
send me a message on D$2$L (preferred).

You can reach Brad (TA) during his office hours in the Student Success Center (Barnard 259):
\begin{itemize}
  \item Monday $15$:$00$-$16$:$00$
  \item Tuesday $10$:$00$-$11$:$00$
\end{itemize}

%------------------------------ Section -------------------------------------
\section*{Course Materials}
All readings, assignments, and handouts will be posted on D$2$L.
Furthermore, all notes will be posted on D$2$L after each lecture.
During in-class activities, worksheets may be provided, but these will
also be made available on D$2$L after the class period.

Readings will come from the following course materials:
\begin{itemize}
	\item Discrete Mathematics: Introduction to Mathematical Reasoning (Brief Edition) by Sussana Epp.
	\item \href{https://www.cs.montana.edu/brittany/teaching/discrete/Book.pdf}{Discrete
	Mathematics Lecture notes from Edelsbrunner and Fasy (EF)}.
	\item \href{https://rob-bell.net/2009/06/a-beginners-guide-to-big-o-notation/}{Big O notation notes from Rob Bell}.
\end{itemize}

%------------------------------ Section -------------------------------------
\section*{Course Outcomes and Objectives}
By the end of this course, a student will:
\begin{itemize}
\item Be able to use formal proof techniques, including mathematical induction and proof by contradiction.
\item Understand algorithmic complexity and be able to use it to compare different program designs for a problem.
\item Solve problems that use logic, sets, and functions.
\item Solve problems using Boolean algebra.
\item Solve problems that use permutations and combinations.
\item Solve problems that use discrete probability.
\item Solve problems that use basic graph theory.
\end{itemize}

%------------------------------ Section -------------------------------------
\section*{Grading}
The course will consist of assignments, random quizzes, and exams.
At the end of the semester I will not entertain requests to ``bump'' up
borderline grades.
The grades will be broken down as follows:
\begin{itemize}
	\item $40\%$ Assignments (top $8$ counted towards grade)
	\item $10\%$ ``Pop'' Quizzes (top $8$ counted towards grade)
	\item $30\%$ Midterm Exams ($2$ Midterm exams, $15\%$ each)
	\item $15\%$ Final Exam
	\item $5\%$ Best Exam Grade (including final)
\end{itemize}

\section*{Exams}
If you need special accomodations for the exams then please let me know
within the first week of the course (by Jan. 16th) so that we can
get in touch with the testing center.

%------------------------------ Section -------------------------------------
\section*{Attendance Policy}
Class attendance and participation is not required.
However, there is no way to make
up a missed in-class ``pop'' quiz. These quizzes will be random and only the top $8$ will
count towards the final grade. This way, one or two
missed class periods due to emergencies or illness
should not negatively affect the final grade.

%------------------------------ Section -------------------------------------
\section*{Homework Policy}
All assignments must be submitted to Gradescope by $23$:$59$ on the day of the due
date. At this time, the dropbox on Gradescope will close and not allow
any more submissions.
\emph{No late assignments will be accepted}.
Submissions to Gradescope must have correct pages selected for grading, otherwise
your problems will \emph{not be graded}.

Assignments (unless otherwise specified) \emph{must} be turned in as a
pdf document. \LaTeX\ is preferred, as there will be extra credit opportunities
throughout the semester for performing specific tasks in \LaTeX\ (i.e., adding
a table, including figures).

Throughout the semester, there will be $10$ total assignments, with $8$ of them
counting towards your overall assignment grade (your lowest two grades
will be dropped at the end of the semester).
The first two assignments will be individually completed, the second two
will be in small groups of $2$-$4$ students, and the remainder
of the assignments you will have the option to choose whether you work in
a group or not (unless otherwise specified).

%------------------------------ Section -------------------------------------
\section*{Academic Integrity and Conduct}
A summary of expectations for students are found at the following link:
\url{http://catalog.montana.edu/code-conduct-policies-regulations-reports/}
and a full write up is found at: \url{https://www.montana.edu/policy/student_conduct/}.
Please familiarize yourselves with the material from both of these urls.
Additionally, the following rules are in place for collaboration and use of
external resources:
\begin{itemize}
	\item When collaborating with other students on homework you \emph{must}
	list all collaborators on that problem in collaborators section of the
	\LaTeX template. All solutions must be written up individually (unless
	otherwise specified), each
	student must demonstrate mastery of the solution submitted. Examples of
	collaboration scenarios and ``what to do'' (these examples are by
	no means exhaustive, please ask the instructor if you find yourself
	in a collaboration scenario that you are not sure how to handle):
	\begin{itemize}
		\item Student A and student B meet with the teaching assistant
		and discuss how to layout a proof and begin formulating the proof,
		together, with the teaching assistant. Both students should list
		each other as collaborators and start from scratch when writing up
		their submissions for the homework to avoid plagiarism and to
		ensure that they fully understand their solutions.
		\item Student A, student B, and student C meet up to work together
		on the homework and work out the solution on a white board. Each student
		should avoid writing up the solution while with the group and write
		it up individually without copying their notes from the group to
		ensure they fully understand the solution being submitted. Each
		student should list the other two as collaborators on the problem.
	\end{itemize}
	\item When consulting with outside resources (i.e., other textbooks,
	google, etc.), students must include a full citation of the resource
	used. For websites, a url is sufficient. Additionally, these resources
	should not be used when writing up the solution, you must demonstrate to
	myself, and yourselves, that you fully understand the solution being submitted.
\end{itemize}
If the previous requirements are not met, the submitted solution may be
considered academic misconduct and will be reported as such.

%------------------------------ Section -------------------------------------
\section*{Withdrawing}
I will not sign documents in class, please schedule a time to meet with me
or come to my office hours.

Withdrawel dates are as follows:
\begin{itemize}
	\item Last day to drop online:  $23$ January $2019$
	\item Last day to drop without a ``W'' Grade: $30$ January $2019$
	\item Last day to drop with a ``W'' Grade: $11$ April $2019$
\end{itemize}
After $30$ January $2019$, I will only support requests to withdraw
from this course with a ``W" grade if you personally meet with me and
we agree that dropping the course is the best option.
If you are considering withdrawing from this class, discussing this with me as
early as possible is advised.

%------------------------------ Section -------------------------------------
\section*{Grade Changes}
Grading mistakes happen, and we are always happy to explain grades on particular
problems. If a mistake is found, we will do our best to quickly
update the grade to reflect
the new score. However, to ensure that grades are kept up to date and
submissions are graded fairly, we will only entertain regrade requests
within two weeks of the grades being published unless otherwise specified.
This policy applies to any and all grading concerns.

%------------------------------ Section -------------------------------------
\section*{Schedule}
Updates to the schedule may happen at anytime throughout the semester.
Updates will be announced in class and the syllabus will be updated
on D$2$L to reflect these changes.
\paragraph{Week 1: Jan. $9$, $11$}
\begin{itemize}
\item Wednesday Jan. $9$: Introduction to course and \LaTeX
  \begin{itemize}
    \item Homework 1 assigned
  \end{itemize}
\item Friday Jan. $11$: Variables and Sets
	\begin{itemize}
		\item Reading: Epp Ch. $1.1$, $1.2$, and $6.1$
	\end{itemize}
\end{itemize}

\paragraph{Week 2: Jan. $14$, $16$, $18$}
\begin{itemize}
\item Monday Jan. $14$: Relations and Functions on Sets
	\begin{itemize}
		\item Reading: Epp Ch. $1.3$ and $7.1$
	\end{itemize}
\item Wednesday Jan. $16$: Logical Statements and Equivalence
	\begin{itemize}
		\item Reading: Epp Ch. $2.1$
	\end{itemize}
\item Friday Jan. $18$: Conditional Statements and Valid/Invalid Arguments
	\begin{itemize}
		\item Reading: Epp Ch. $2.2$ and $2.3$
		\item In class exercise
	\end{itemize}
\end{itemize}

\paragraph{Week 3: Jan. $21$, $23$, $25$}
\begin{itemize}
\item Monday Jan. $21$: MLK Day \textbf{No class}
\item Wednesday Jan. $23$: Quantified Statements
	\begin{itemize}
                \item \emph{Homework 1 due}
                \item Homework 2 assigned
		\item Reading: Epp Ch. $3.1$, $3.2$ and $3.3$
	\end{itemize}
\item Friday Jan. $25$: Arguments with Quantified Statements
	\begin{itemize}
		\item Reading: Epp Ch. $3.4$
		\item In class exercise
	\end{itemize}
\end{itemize}

\paragraph{Week 4: Jan. $28$, $30$, Feb. $1$}
\begin{itemize}
\item Monday Jan. $28$: Direct Proof and Counterexamples
	\begin{itemize}
		\item Reading: Epp Ch. $4.1$ and $4.2$
	\end{itemize}
\item Wednesday Jan. $30$: Divisibility and Quotient Remainder Theorem
	\begin{itemize}
                \item \emph{Homework 2 due}
		\item Reading: Epp Ch. $4.3$ and $4.4$
		\item Material for Midterm $1$ ends here
	\end{itemize}
\item Friday Feb. $1$: In class exercise (Applying things we've learned)
\end{itemize}

\paragraph{Week 5: Feb. $4$, $6$, $8$}
\begin{itemize}
\item Monday Feb. $4$: \textbf{Midterm $1$}
	\begin{itemize}
		\item Covers all material before Feb. $1$
	\end{itemize}
\item Wednesday Feb. $6$: Indirect Arguments: Proof by Contradiction and Contraposition
	\begin{itemize}
                \item Homework 3 assigned
		\item Reading: Epp Ch. $4.5$ and $4.6$
	\end{itemize}
\item Friday Feb. $8$: Sequences
	\begin{itemize}
		\item Reading: Epp Ch. $5.1$
	\end{itemize}
\end{itemize}

\paragraph{Week 6: Feb. $11$, $13$, $15$}
\begin{itemize}
\item Monday Feb. $11$: Induction
	\begin{itemize}
		\item Reading: Epp Ch. $5.2$ and $5.3$
	\end{itemize}
\item Wednesday Feb. $13$: Induction
	\begin{itemize}
		\item Reading: Epp Ch. $5.2$ and $5.3$
	\end{itemize}
\item Friday Feb. $15$: Induction cnt., Strong Induction, and Reccurrence Relations
	\begin{itemize}
		\item Reading: Epp Ch. $5.4$
	\end{itemize}
\end{itemize}

\paragraph{Week 7: Feb. $18$, $20$, $22$}
\begin{itemize}
\item Monday Feb. $18$: President's Day \textbf{No class}
\item Wednesday Feb. $20$: Recurrence Relations, Induction, and Algorithms
	\begin{itemize}
                \item \emph{Homework 3 due}
                \item Homework 4 assigned
		\item Reading: Epp Ch. $5.5$ and $5.6$
		\item Edelsbrunner and Fasy Ch. $12$ and $13$
	\end{itemize}
\item Friday Feb. $22$: Algorithms and Growth Rates Cnt.
	\begin{itemize}
		\item Reading Edelsbrunner and Fasy Ch. $13$
	\end{itemize}
\end{itemize}

\paragraph{Week 8: Feb. $25$, $27$, Mar. $1$}
\begin{itemize}
\item Monday Feb. $25$: Complexity
	\begin{itemize}
		\item Reading: Reading Edelsbrunner and Fasy Ch. $9$
		\item Rob Bell notes: \url{https://rob-bell.net/2009/06/a-beginners-guide-to-big-o-notation/}
	\end{itemize}
\item Wednesday Feb. $27$: Algorithms and Compelity Cnt.
	\begin{itemize}
                \item \emph{Homework 4 due}
                \item Homework 5 assigned
		\item Reading: Epp Ch. $8.5$
	\end{itemize}
\item Friday Mar. $1$: Master Theorem
	\begin{itemize}
		\item In class exercise: Reading Edelsbrunner and Fasy Ch. $14$
	\end{itemize}
\end{itemize}

\paragraph{Week 9: Mar. $4$, $6$, $8$}
\begin{itemize}
\item Monday Mar. $4$: Algorithm Correctness [not on Exams]
	\begin{itemize}
		\item Reading Introduction to Algorithms 3rd Ed. (Corment, Leiserson, Rivest, and Stein - 2009)
	\end{itemize}
\item Wednesday Mar. $6$: Algorithm Correctness Cnt. [not on Exams]
	\begin{itemize}
                \item \emph{Homework 5 due}
		\item Reading Introduction to Algorithms 3rd Ed. (Corment, Leiserson, Rivest, and Stein - 2009)
	\end{itemize}
\item Friday Mar. $8$: In class exercise (Applying things we've learned)
\end{itemize}

\paragraph{Week 10: Mar. $11$, $13$, $15$}
\begin{itemize}
\item Monday Mar. $11$: \textbf{Midterm 2}
	\begin{itemize}
		\item Covers all Material after Feb. $1$ and before Mar. $8$
	\end{itemize}
\item Wednesday Mar. $13$: Introduction to Counting
	\begin{itemize}
                \item Homework 6 assigned
		\item Reading: Epp Ch. $9.1$, $9.2$, and $9.3$
	\end{itemize}
\item Friday Mar. $15$: Counting cnt. (Permutations)
	\begin{itemize}
		\item In class exercise
	\end{itemize}
\end{itemize}

\paragraph{Week 11: Mar. $18$, $20$, $22$}
\begin{itemize}
\item Monday Mar. $18$: Spring Break \textbf{No class}
\item Wednesday Mar. $20$: Spring Break \textbf{No class}
\item Friday Mar. $22$: Spring Break \textbf{No class}
\end{itemize}

\paragraph{Week 12: Mar. $25$, $27$, $29$}
\begin{itemize}
\item Monday Mar. $25$: Pigeon Hole Principle
	\begin{itemize}
		\item Reading: Epp Ch. $9.4$
	\end{itemize}
\item Wednesday Mar. $27$: Combinations and Binomial Coefficients
	\begin{itemize}
                \item \emph{Homework 6 due}
                \item Homework 7 assigned
		\item Reading: Epp Ch. $9.5$
	\end{itemize}
\item Friday Mar. $29$: Introduction to Probability
	\begin{itemize}
    \item Reading: Probability and Statistics for Engineering and the Sciences
      7th edition (Devore) Ch.$2.4$ and $2.5$
		\item In class exercise
	\end{itemize}
\end{itemize}

\paragraph{Week 13: Apr. $1$, $3$, $5$}
\begin{itemize}
\item Monday Apr. $1$: Random Variables and Expected Value
  \begin{itemize}
    \item EF Ch. $17$
  \end{itemize}
\item Wednesday Apr. $3$: Distributions and Variance
	\begin{itemize}
                \item \emph{Homework 7 due}
                \item Homework 8 assigned
		\item Reading EF Ch. $19$
	\end{itemize}
\item Friday Apr. $5$: Probability in Hashing
	\begin{itemize}
		\item Reading: EF Ch. $18$
	\end{itemize}
\end{itemize}

\paragraph{Week 14: Apr. $8$, $10$, $12$}
\begin{itemize}
\item Monday Apr. $8$: Relations
	\begin{itemize}
		\item Reading: Epp Ch. $8.1$ and $8.2$
	\end{itemize}
\item Wednesday Apr. $10$: Equivalence Relations
	\begin{itemize}
                \item \emph{Homework 8 due}
                \item Homework 9 assigned
		\item Reading: Epp Ch. $8.3$ and $8.4$
	\end{itemize}
\item Friday Apr. $12$: Equivalence Relations Cnt.
	\begin{itemize}
		\item Epp Ch. $8.3$ and $8.4$
	\end{itemize}
\end{itemize}

\paragraph{Week 15: Apr. $15$, $17$, $19$}
\begin{itemize}
\item Monday Apr. $15$: Graphs
	\begin{itemize}
		\item Reading: Epp Ch. $10.1$
	\end{itemize}
\item Wednesday Apr. $17$: Trails, Paths, and Circuits
	\begin{itemize}
		\item Reading: Epp Ch. $10.2$
                \item Homework 10 assigned
	\end{itemize}
\item Friday Apr. $19$: University Day \textbf{No class}
\end{itemize}

\paragraph{Week 16: Apr. $22$, $24$, $26$}
\begin{itemize}
\item Monday Apr. $22$: Trees
	\begin{itemize}
		\item Reading: Epp Ch. $10.3$ and $10.4$
	\end{itemize}
\item Wednesday Apr. $24$: Proofs on Trees and Applications
        \begin{itemize}
                \item \emph{Homework 9 due}
                \item \emph{Homework 10 due}
        \end{itemize}
\item Friday Apr. $26$: Last day of class, reflection period
\end{itemize}

\paragraph{Week 17: Finals Week}
\begin{itemize}
\item Finals Time/Location May 2nd, 6:00-7:50pm, normal classroom.
\end{itemize}

\end{document}

